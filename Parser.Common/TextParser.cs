﻿using Parser.DataContract.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Web;

namespace Parser.Common
{
    public static class TextParser
    {
        public static List<ParsingDataModel> ParseText(List<string> textData)
        {
            var result = new List<ParsingDataModel>();

            foreach (var row in textData)
            {
                var textItem = row.Split(' ');
                if (textItem.Length != 10)
                    continue;
                try
                {
                    int paramIndex = textItem[6].IndexOf('?');
                    string parameters = paramIndex == -1 ? string.Empty : textItem[6].Substring(paramIndex);
                    string route = parameters.Length > 0 ? textItem[6].Replace(parameters, string.Empty) : textItem[6];
                    //можно заменить регуляркой с проверкой на конкретные типы файлов
                    int extIndex = route.LastIndexOf('.');
                    //для упрощения допускаем что файл может иметь расширение от 2х до 4х символов (js/html)
                    if (extIndex <= textItem[6].Length - 3 && extIndex >= textItem[6].Length - 5)
                        continue;

                    string hostName = textItem[0];
                    string dateStr = textItem[3].Replace("[", string.Empty) + textItem[4].Replace("]", string.Empty);
                    DateTime dt = DateTime.ParseExact(dateStr, "dd/MMM/yyyy:HH:mm:sszzzz", CultureInfo.InvariantCulture).ToUniversalTime();

                    int size;
                    Int32.TryParse(textItem[9], out size);
                    int resultCode;
                    Int32.TryParse(textItem[8], out resultCode);
                    result.Add(ParsingDataModel.CreateNew(hostName, dt, route, parameters, size, resultCode));

                }
                catch (Exception ex)
                {
                    //log parse exceptions
                }
            }

            return result;
        }
    }
}
