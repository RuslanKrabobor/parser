﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Parser.Common
{
    public class LocationLocator
    {
        private const string _undefined = "undefined";
        private readonly string _url;
        private readonly string _key;
        private HttpClient _client;
        public LocationLocator()
        {
            _url = System.Configuration.ConfigurationManager.AppSettings["ipstackUrl"];
            _key = System.Configuration.ConfigurationManager.AppSettings["ipstackApikey"];
            _client = new HttpClient();
        }

        // можно заменить на любой другой сервис который поддерживает балк реквесты по 
        public async Task<Dictionary<string, string>> GetLocationsByHostNames(IEnumerable<string> hosts)
        {
            var result = new Dictionary<string, string>();
            foreach (var host in hosts)
            {
                string response = await _client.GetStringAsync(GetUrl(host));
                JObject jo = JObject.Parse(response);
                string loction;
                try
                {
                    string country = (string)jo["country_name"];
                    string city = (string)jo["city"];
                    loction = $"{(country == null ? _undefined : country)}, {(city == null ? _undefined : city)}";
                }
                catch (Exception ex)
                {
                    // log some parse exception
                    loction = _undefined;
                }
                result.Add(host, loction);
            }

            return result;
        }

        private string GetUrl(string host)
        {
            return $"{_url}/{host}?access_key={_key}";
        }
    }

}
