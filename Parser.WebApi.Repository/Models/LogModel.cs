﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parser.Ef;

namespace Parser.WebApi.Repository.Models
{
    public class LogModel
    {
        private LogModel(
            string host,
            DateTime date,
            string route,
            string parameters,
            int size,
            int result,
            string location)
        {
            Host = host;
            Date = date;
            Route = route;
            Parameters = parameters;
            Size = size;
            Result = result;
            Location = location;

        }

        public string Host { get; private set; }

        public DateTime Date { get; private set; }

        public string Route { get; private set; }

        public string Parameters { get; private set; }

        public int Size { get; private set; }

        public int Result { get; private set; }

        public string Location { get; private set; }

        public static LogModel CreateNew(ParsingData item)
        {
            return new LogModel(
                item.HostName,
                item.RequestDate,
                item.Route,
                item.Parameters,
                item.Size,
                item.Result,
                item.Location);
        }
    }
}
