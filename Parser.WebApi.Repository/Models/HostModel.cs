﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parser.WebApi.Repository.Models
{
    public class HostModel
    {
        private HostModel(string host, int count)
        {
            Host = host;
            Count = count;
        }

        public string Host { get; private set; }

        public int Count { get; private set; }

        public static HostModel CreateNew(string host, int count)
        {
            return new HostModel(
                host,
                count);
        }
    }
}
