﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parser.WebApi.Repository.Models
{
    public class RouteModel
    {
        private RouteModel(string route, int count)
        {
            Route = route;
            Count = count;
        }

        public string Route { get; private set; }

        public int Count { get; private set; }

        public static RouteModel CreateNew(string route, int count)
        {
            return new RouteModel(
                route,
                count);
        }
    }
}
