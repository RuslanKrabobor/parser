﻿using Parser.Ef;
using Parser.WebApi.Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Parser.WebApi.Repository.Repositories
{
    //Интерфейсы и DI не используются по причине ускорения написания кода и тестовой направленности проекта
    public class PerserRepository
    {
        public async Task<List<HostModel>> GetHosts(int n, DateTime? start, DateTime? end)
        {
            var result = new List<HostModel>();
            using (var ctx = new parserDbEntities())
            {
                IQueryable<ParsingData> dataSet = SetFilterByDate(start, end, ctx);

                var dataModels = dataSet.GroupBy(x => x.HostName).Select(g => new
                {
                    Host = g.Key,
                    Count = g.Count()
                }).OrderByDescending(y => y.Count).Take(n);

                foreach (var item in dataModels)
                    result.Add(HostModel.CreateNew(item.Host, item.Count));
            }
            return result;
        }

        public async Task<List<RouteModel>> GetRoutes(int n, DateTime? start, DateTime? end)
        {
            var result = new List<RouteModel>();
            using (var ctx = new parserDbEntities())
            {
                IQueryable<ParsingData> dataSet = SetFilterByDate(start, end, ctx);

                var dataModels = dataSet.GroupBy(x => x.Route).Select(g => new
                {
                    Route = g.Key,
                    Count = g.Count()
                }).OrderByDescending(y => y.Count).Take(n);

                foreach (var item in dataModels)
                    result.Add(RouteModel.CreateNew(item.Route, item.Count));
            }
            return result;
        }

        public async Task<List<LogModel>> GetLogs(DateTime? start, DateTime? end, int offset, int limit)
        {
            var result = new List<LogModel>();
            using (var ctx = new parserDbEntities())
            {
                IQueryable<ParsingData> dataSet = SetFilterByDate(start, end, ctx);

                dataSet = dataSet.OrderBy(x => x.RequestDate);

                if (offset > 0)
                    dataSet = dataSet.Skip(offset);

                var dataModels = dataSet.Take(limit);

                foreach (var item in dataModels)
                    result.Add(LogModel.CreateNew(item));
            }
            return result;
        }

        private IQueryable<ParsingData> SetFilterByDate(DateTime? start, DateTime? end, parserDbEntities ctx)
        {
            IQueryable<ParsingData> dataSet = ctx.ParsingDatas;
            if (start != null)
                dataSet = dataSet.Where(x => x.RequestDate >= start.Value);

            if (end != null)
                dataSet = dataSet.Where(x => x.RequestDate <= end.Value);
            return dataSet;
        }
    }
}
