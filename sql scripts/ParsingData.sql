SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ParsingData](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HostName] [nvarchar](max) NOT NULL,
	[RequestDate] [datetime] NOT NULL,
	[Route] [nvarchar](max) NOT NULL,
	[Parameters] [nvarchar](max) NOT NULL,
	[Size] [int] NOT NULL,
	[Result] [int] NOT NULL,
	[Location] [nvarchar](max) NOT NULL,
	[ParsingEventId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[ParsingData]  WITH CHECK ADD  CONSTRAINT [FK_ParsingData_ParsingEvent] FOREIGN KEY([ParsingEventId])
REFERENCES [dbo].[ParsingEvent] ([Id])
GO

ALTER TABLE [dbo].[ParsingData] CHECK CONSTRAINT [FK_ParsingData_ParsingEvent]
GO