﻿using Parser.DataContract.Models;
using Parser.Ef;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Parser.DataContract.Repositories
{
    //Интерфейсы и DI не используются по причине ускорения написания кода и тестовой направленности проекта
    public class ParsingDataRepository
    {
        public async Task Create(List<ParsingDataModel> models)
        {
            using (var ctx = new parserDbEntities())
            {
                IEnumerable<ParsingData> dataModels = models.Select(x => MapModelToDataModel(x));
                ctx.ParsingDatas.AddRange(dataModels);
                await ctx.SaveChangesAsync();
            }
        }

        private static ParsingData MapModelToDataModel(ParsingDataModel model)
        {
            return new ParsingData
            {
                Id = model.Id,
                HostName = model.HostName,
                RequestDate = model.RequestDate,
                Route = model.Route,
                Parameters = model.Parameters,
                Size = model.Size,
                Result = model.Result,
                Location = model.Location,
                ParsingEventId = model.ParsingEventId
            };
        }
    }
}
