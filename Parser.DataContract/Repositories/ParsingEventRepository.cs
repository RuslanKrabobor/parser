﻿using Parser.DataContract.Models;
using Parser.Ef;
using System.Threading.Tasks;

namespace Parser.DataContract.Repositories
{
    //Интерфейсы и DI не используются по причине ускорения написания кода и тестовой направленности проекта
    public class ParsingEventRepository
    {
        public async Task<int> Create(ParsingEventModel model)
        {
            ParsingEvent dataModel = MapModelToDataModel(model);
            using (var ctx = new parserDbEntities())
            {
                ctx.ParsingEvents.Add(dataModel);
                await ctx.SaveChangesAsync();
            }
            return dataModel.Id;
        }

        private static ParsingEvent MapModelToDataModel(ParsingEventModel model)
        {
            return new ParsingEvent
            {
                Id = model.Id,
                MachineName = model.MachineName,
                Date = model.Date
            };
        }
    }
}
