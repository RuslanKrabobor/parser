﻿using Parser.Ef;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parser.DataContract.Models
{
    public class ParsingDataModel
    {
        public ParsingDataModel(ParsingData dataModel)
           : this(
                 dataModel.Id,
                 dataModel.HostName,
                 dataModel.RequestDate,
                 dataModel.Route,
                 dataModel.Parameters,
                 dataModel.Size,
                 dataModel.Result,
                 dataModel.Location,
                 dataModel.ParsingEventId)
        {
        }
        private ParsingDataModel(int id,
            string hostName,
            DateTime requestDate,
            string route,
            string parameters,
            int size, 
            int result,
            string location,
            int? parsingEventId)
        {
            Id = id;
            HostName = hostName;
            RequestDate = requestDate;
            Route = route;
            Parameters = parameters;
            Size = size;
            Result = result;
            Location = location;
            ParsingEventId = parsingEventId;

        }
        public int Id { get; private set; }
        public string HostName { get; private set; }
        public DateTime RequestDate { get; private set; }
        public string Route { get; private set; }
        public string Parameters { get; private set; }
        public int Size { get; private set; }
        public int Result { get; private set; }
        public string Location { get; private set; }
        public int? ParsingEventId { get; private set; }

        public void SetLocation(string value)
        {
            Location = value;
        }
        public void SetParsingEvent(int id)
        {
            ParsingEventId = id;
        }

        public static ParsingDataModel CreateNew(string hostName,
            DateTime requestDate,
            string route,
            string parameters,
            int size,
            int result)
        {
            return new ParsingDataModel(
                0,
                hostName,
                requestDate,
                route,
                parameters,
                size,
                result,
                string.Empty,
                null);
        }
    }
}
