﻿using Parser.Ef;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parser.DataContract.Models
{
    public class ParsingEventModel
    {

        public ParsingEventModel(ParsingEvent dataModel)
           : this(
                 dataModel.Id,
                 dataModel.MachineName,
                 dataModel.Date)
        {
        }
        private ParsingEventModel(int id,
            string machineName,
            DateTime date)
        {
            Id = id;
            MachineName = machineName;
            Date = date;
        }

        public int Id { get; private set; }
        public string MachineName { get; private set; }
        public DateTime Date { get; private set; }

        public static ParsingEventModel CreateNew(string name, DateTime date)
        {
            return new ParsingEventModel(
                0,
                name,
                date);
        }
    }
}
