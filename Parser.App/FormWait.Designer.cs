﻿namespace Parser.App
{
    partial class FormWait
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.loaderGif = new System.Windows.Forms.PictureBox();
            this.labelLoader = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.loaderGif)).BeginInit();
            this.SuspendLayout();
            // 
            // loaderGif
            // 
            this.loaderGif.Image = global::Parser.App.Properties.Resources.giphy_min;
            this.loaderGif.Location = new System.Drawing.Point(13, 13);
            this.loaderGif.Name = "loaderGif";
            this.loaderGif.Size = new System.Drawing.Size(67, 64);
            this.loaderGif.TabIndex = 0;
            this.loaderGif.TabStop = false;
            // 
            // labelLoader
            // 
            this.labelLoader.AutoSize = true;
            this.labelLoader.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLoader.Location = new System.Drawing.Point(87, 34);
            this.labelLoader.Name = "labelLoader";
            this.labelLoader.Size = new System.Drawing.Size(291, 24);
            this.labelLoader.TabIndex = 1;
            this.labelLoader.Text = "Parsing in progress. Please wait...";
            // 
            // FormWait
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 89);
            this.ControlBox = false;
            this.Controls.Add(this.labelLoader);
            this.Controls.Add(this.loaderGif);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormWait";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this.loaderGif)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox loaderGif;
        private System.Windows.Forms.Label labelLoader;
    }
}