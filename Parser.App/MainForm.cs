﻿using Parser.Common;
using Parser.DataContract.Models;
using Parser.DataContract.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Parser.App
{
    public partial class MainForm : Form
    {
        private ParsingEventRepository _parsingEventRepository;
        private ParsingDataRepository _parsingDataRepository;
        public MainForm()
        {
            InitializeComponent();
            _parsingEventRepository = new ParsingEventRepository();
            _parsingDataRepository = new ParsingDataRepository();
        }

        private void buttonOpenFile_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    textBoxFilePath.Text = openFileDialog.FileName;
                }
            }
        }
        // бизнес логика не вынесена в отдельный слой и находится в коде формы по причине упрощения кода тестового проекта и разового использования
        private async void buttonParse_Click(object sender, EventArgs e)
        {
            string filePath = textBoxFilePath.Text;
            if (filePath.Length == 0)
            {
                MessageBox.Show("Please enter a file location");
                return;
            }

            if (!File.Exists(filePath))
            {
                MessageBox.Show("File does not exist.");
                return;
            }

            var loader = new FormWait();
            loader.Show(this);
            Enabled = false;
            try
            {
                List<string> textList = ReadDataFromFile(filePath);
                List<ParsingDataModel> parserData = ParseData(textList);
                if (parserData.Count == 0)
                    return;
                await SetLocation(parserData);
                await SetDataToDb(parserData);
            }
            finally
            {
                Enabled = true;
                loader.Close();
            }

            textBoxFilePath.Text = string.Empty;
            MessageBox.Show("Done!");
        }

        private async Task SetLocation(List<ParsingDataModel> parserData)
        {
            var locator = new LocationLocator();
            var hosts = parserData.Select(x => x.HostName).Distinct();
            var locations = await locator.GetLocationsByHostNames(hosts);
            foreach(var location in locations)
            {
                var items = parserData.Where(x => x.HostName == location.Key);
                foreach (var item in items) 
                    item.SetLocation(location.Value);
            }
        }

        private async Task SetDataToDb(List<ParsingDataModel> parserData)
        {
            var eventId = await _parsingEventRepository.Create(ParsingEventModel.CreateNew(Environment.MachineName, DateTime.UtcNow));
            foreach(var item in parserData)
                item.SetParsingEvent(eventId);
            await _parsingDataRepository.Create(parserData);
        }

        private static List<ParsingDataModel> ParseData(List<string> textList)
        {
            List<ParsingDataModel> parserData = TextParser.ParseText(textList);
            if (parserData.Count == 0)
                MessageBox.Show("No valid data in file");
            return parserData;
        }

        private static List<string> ReadDataFromFile(string filePath)
        {
            List<string> textList = new List<string>();

            using (StreamReader fs = new StreamReader(filePath))
            {
                while (true)
                {
                    string temp = fs.ReadLine();
                    if (temp == null)
                        break;

                    textList.Add(temp);
                }
            }

            return textList;
        }
    }
}
