﻿using Newtonsoft.Json;
using Parser.WebApi.Converters;
using Parser.WebApi.Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Parser.WebApi.Models
{
    public class LogResponseModel
    {
        public LogResponseModel(
            string host,
            DateTime date,
            string route,
            string parameters,
            int size,
            int result,
            string location)
        {
            Host = host;
            Date = date;
            Route = route;
            Parameters = parameters;
            Size = size;
            Result = result;
            Location = location;

        }
        [JsonProperty("host")]
        public string Host { get; private set; }

        [JsonProperty("date")]
        [JsonConverter(typeof(JsonDateTimeConverter))]
        public DateTime Date { get; private set; }

        [JsonProperty("route")]
        public string Route { get; private set; }

        [JsonProperty("params")]
        public string Parameters { get; private set; }

        [JsonProperty("size")]
        public int Size { get; private set; }

        [JsonProperty("statusCode")]
        public int Result { get; private set; }

        [JsonProperty("location")]
        public string Location { get; private set; }

        public static LogResponseModel ConvertToModel(LogModel model)
        {
            return new LogResponseModel(model.Host,
                model.Date,
                model.Route,
                model.Parameters,
                model.Size,
                model.Result,
                model.Location);
        }
    }
}