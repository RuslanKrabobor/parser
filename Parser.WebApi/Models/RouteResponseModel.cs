﻿using Newtonsoft.Json;
using Parser.WebApi.Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Parser.WebApi.Models
{
    public class RouteResponseModel
    {
        public RouteResponseModel(string route, int count)
        {
            Route = route;
            Count = count;
        }
        [JsonProperty("route")]
        public string Route { get; private set; }

        [JsonProperty("count")]
        public int Count { get; private set; }

        public static RouteResponseModel ConvertToModel(RouteModel model)
        {
            return new RouteResponseModel(model.Route, model.Count);
        }
    }
}