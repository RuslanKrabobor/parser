﻿using Newtonsoft.Json;
using Parser.WebApi.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Parser.WebApi.Models
{
    public class ProbeResponseModel
    {

        public ProbeResponseModel(string status,
            string message,
            DateTime date)
        {
            Status = status;
            Message = message;
            Date = date;
        }

        [JsonProperty("status")]
        public string Status { get; private set; }

        [JsonProperty("message")]
        public string Message { get; private set; }

        [JsonProperty("date")]
        [JsonConverter(typeof(JsonDateTimeConverter))]
        public DateTime Date { get; private set; }
    }
}