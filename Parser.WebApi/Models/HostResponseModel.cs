﻿using Newtonsoft.Json;
using Parser.WebApi.Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Parser.WebApi.Models
{
    public class HostResponseModel
    {
        public HostResponseModel(string host, int count)
        {
            Host = host;
            Count = count;
        }

        [JsonProperty("host")]
        public string Host { get; private set; }

        [JsonProperty("count")]
        public int Count { get; private set; }

        public static HostResponseModel ConvertToModel(HostModel model)
        {
            return new HostResponseModel(model.Host, model.Count);
        }
    }
}