﻿using Parser.WebApi.Models;
using Parser.WebApi.Repository.Models;
using Parser.WebApi.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Parser.WebApi.Controllers
{
    [RoutePrefix("api/parser")]
    public class ParserController : ApiController
    {
        private PerserRepository _parserRepo;
        public ParserController()
        {
            _parserRepo = new PerserRepository();
        }

        [HttpGet]
        [Route("")]
        public ProbeResponseModel HealthCheck()
        {
            return new ProbeResponseModel ("Ok", "Everything is fine.",DateTime.Now);
        }

        [HttpGet]
        [Route("hosts")]
        public async Task<IEnumerable<HostResponseModel>> GetHosts(int n = 10, DateTime? start = null, DateTime? end = null)
        {
            List<HostModel> models = await _parserRepo.GetHosts(n, start, end);
            return models.Select(x => HostResponseModel.ConvertToModel(x));
        }

        [HttpGet]
        [Route("routes")]
        public async Task<IEnumerable<RouteResponseModel>> GetRoutesAsync(int n = 10, DateTime? start = null, DateTime? end = null)
        {
            List<RouteModel> models = await _parserRepo.GetRoutes(n, start, end);
            return models.Select(x => RouteResponseModel.ConvertToModel(x));
        }

        [HttpGet]
        [Route("logs")]
        public async Task<IEnumerable<LogResponseModel>> GetLogsAsync(DateTime? start = null, DateTime? end = null, int offset = 0, int limit = 10)
        {
            List<LogModel> models = await _parserRepo.GetLogs(start, end, offset, limit);
            return models.Select(x => LogResponseModel.ConvertToModel(x));    
        }
    }
}
